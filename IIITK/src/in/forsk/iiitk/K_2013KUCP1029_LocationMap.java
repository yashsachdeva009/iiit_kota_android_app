package in.forsk.iiitk;

import java.io.IOException;
import java.io.InputStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

//set the target in the emulator to google apis
public class K_2013KUCP1029_LocationMap extends Activity implements OnClickListener {
	String lat, lng;
	LatLng mypos;
	private GoogleMap googleMap;
	JSONArray mjsonarray;
	JSONObject obj;
	RelativeLayout hoffice, library, audi, playg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_k2013kucp1029_location_map);

		hoffice = (RelativeLayout) findViewById(R.id.hoffice);
		library = (RelativeLayout) findViewById(R.id.library);
		audi = (RelativeLayout) findViewById(R.id.audi);
		playg = (RelativeLayout) findViewById(R.id.playground);

		hoffice.setOnClickListener(this);
		library.setOnClickListener(this);
		audi.setOnClickListener(this);
		playg.setOnClickListener(this);

		try {
			mjsonarray = new JSONArray(loadJSONFromAsset());
			obj = mjsonarray.getJSONObject(2);
			lat = (String) obj.getString("latitude");
			lng = (String) obj.getString("longitude");
		} catch (JSONException e) {
				e.printStackTrace();
		}
		mypos = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
		try {
			if (googleMap == null) {
				googleMap = ((MapFragment) getFragmentManager()
						.findFragmentById(R.id.map)).getMap();
			}
			googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
			Marker TP = googleMap.addMarker(new MarkerOptions().position(mypos)
					.title("Here"));
			googleMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String loadJSONFromAsset() {
		String json = null;
		try {

			InputStream is = getAssets().open("locationmap.json");

			int size = is.available();

			byte[] buffer = new byte[size];

			is.read(buffer);

			is.close();

			json = new String(buffer, "UTF-8");

		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
		return json;

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		try {
			RelativeLayout btn = (RelativeLayout) v;
			switch (btn.getId()) {
			case R.id.hoffice:
				obj = mjsonarray.getJSONObject(2);
				break;
			case R.id.audi:
				obj = mjsonarray.getJSONObject(1);

				break;
			case R.id.library:
				obj = mjsonarray.getJSONObject(0);

				break;
			case R.id.playground:
				obj = mjsonarray.getJSONObject(3);

			}
			lat = (String) obj.getString("latitude");
			lng = (String) obj.getString("longitude");

		} catch (JSONException e) {

		}
		mypos = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
		try {
			if (googleMap == null) {
				googleMap = ((MapFragment) getFragmentManager()
						.findFragmentById(R.id.map)).getMap();
			}
			googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
			Marker TP = googleMap.addMarker(new MarkerOptions().position(mypos)
					.title("Here"));
			googleMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

// AIzaSyBGtatjEw8n2u8hy5Hn7HrI938sZvlF14Q