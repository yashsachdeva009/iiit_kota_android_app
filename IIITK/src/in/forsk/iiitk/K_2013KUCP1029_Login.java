package in.forsk.iiitk;

import java.io.IOException;
import java.io.InputStream;

import org.json.JSONException;
import org.json.JSONObject;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;



public class K_2013KUCP1029_Login extends Activity {
	 static String TWITTER_CONSUMER_KEY = "NXZQ97hzJT0MxWz5gSpz5bQGl";
	    static String TWITTER_CONSUMER_SECRET = "hdyCgS78e4PfsU0Js1ZlqH6qUDFiyaFlYEnXtPt6hqFqNOcxTn";
	    
	    // Preference Constants
	    static String PREFERENCE_NAME = "twitter_oauth";
	    static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
	    static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
	    static final String PREF_KEY_TWITTER_LOGIN = "isTwitterLogedIn";

	    static final String TWITTER_CALLBACK_URL = "oauth://t4jsample";

	    // Twitter oauth urls
	    static final String URL_TWITTER_AUTH = "auth_url";
	    static final String URL_TWITTER_OAUTH_VERIFIER = "oauth_verifier";
	    static final String URL_TWITTER_OAUTH_TOKEN = "oauth_token";

	    Button btnLoginTwitter;
	    
	    Button btnUpdateStatus;
	    
	    Button btnLogoutTwitter;
	    
	    TextView txtUpdate;
	    
	    TextView lblUpdate;
	    TextView lblUserName;

	    ProgressDialog pDialog;

	    private static Twitter twitter;
	    private static RequestToken requestToken;

	    // Shared Preferences
	    private static SharedPreferences mSharedPreferences;
	    
	    String messageToPost;
	    JSONObject obj;

	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_k2013kucp1029_login);
	        
	        Bundle extras = getIntent().getExtras();
			messageToPost = extras.getString("app_link");
	        
	        
	        btnLoginTwitter = (Button) findViewById(R.id.btnLoginTwitter);
	        btnUpdateStatus = (Button) findViewById(R.id.btnUpdateStatus);
	        btnLogoutTwitter = (Button) findViewById(R.id.btnLogoutTwitter);
	        txtUpdate = (TextView)findViewById(R.id.txtUpdateStatus);
	        lblUpdate = (TextView) findViewById(R.id.lblUpdate);
	        lblUserName = (TextView) findViewById(R.id.lblUserName);

	        mSharedPreferences = getApplicationContext().getSharedPreferences(
	                "MyPref", 0);
	        
	        
	        btnLoginTwitter.setOnClickListener(new View.OnClickListener() {

	            @Override
	            public void onClick(View arg0) {
	                // Call login twitter function
	                loginToTwitter();
	            }
	        });

	        /**
	         * Button click event to Update Status, will call updateTwitterStatus()
	         * function
	         * */
	        btnUpdateStatus.setOnClickListener(new View.OnClickListener() {

	            @Override
	            public void onClick(View v) {
	                // Call update status function
	                // Get the status from EditText
	                txtUpdate.setText(messageToPost);
	                String status = txtUpdate.getText().toString();

	                    new updateTwitterStatus().execute(status);

	            }
	        });

	        /**
	         * Button click event for logout from twitter
	         * */
	        btnLogoutTwitter.setOnClickListener(new View.OnClickListener() {

	            @Override
	            public void onClick(View arg0) {
	                // Call logout twitter function
	                logoutFromTwitter();
	            }
	        });

	        /** This if conditions is tested once is
	         * redirected from twitter page. Parse the uri to get oAuth
	         * Verifier
	         * */
	        if (!isTwitterLoggedInAlready()) {
	            Uri uri = getIntent().getData();
	            if (uri != null && uri.toString().startsWith(TWITTER_CALLBACK_URL)) {
	                // oAuth verifier
	                String verifier = uri
	                        .getQueryParameter(URL_TWITTER_OAUTH_VERIFIER);

	                try {
	                    // Get the access token
	                    AccessToken accessToken = twitter.getOAuthAccessToken(
	                            requestToken, verifier);

	                    // Shared Preferences
	                    SharedPreferences.Editor e = mSharedPreferences.edit();

	                    // After getting access token, access token secret
	                    // store them in application preferences
	                    e.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
	                    e.putString(PREF_KEY_OAUTH_SECRET,
	                            accessToken.getTokenSecret());
	                    // Store login status - true
	                    e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
	                    e.commit(); // save changes

	                    // Hide login button
	                    btnLoginTwitter.setVisibility(View.GONE);

	                    // Show Update Twitter
	                    lblUpdate.setVisibility(View.VISIBLE);
	                    txtUpdate.setVisibility(View.VISIBLE);
	                    btnUpdateStatus.setVisibility(View.VISIBLE);
	                    //btnLogoutTwitter.setVisibility(View.VISIBLE);

	                    // Getting user details from twitter
	                    // For now i am getting his name only
	                    long userID = accessToken.getUserId();
	                    User user = twitter.showUser(userID);
	                    String username = user.getName();

	                    // Displaying in xml ui
	                    lblUserName.setText(Html.fromHtml("<b>Welcome " + username + "</b>"));
	                } catch (Exception e) {
	                    // Check log for login errors
	                    Log.e("Twitter Login Error", "> " + e.getMessage());
	                }
	            }
	        }

	    }

	    /**
	     * Function to login twitter
	     * */

	    class LoginHere extends
	            AsyncTask<String,String,String>{

	        @Override
	        protected String doInBackground(String... params) {
	            ConfigurationBuilder builder = new ConfigurationBuilder();
	            builder.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
	            builder.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);
	            Configuration configuration = builder.build();

	            TwitterFactory factory = new TwitterFactory(configuration);
	            twitter = factory.getInstance();

	            try {
	                requestToken = twitter
	                        .getOAuthRequestToken(TWITTER_CALLBACK_URL);
	                startActivity(new Intent(Intent.ACTION_VIEW, Uri
	                        .parse(requestToken.getAuthenticationURL())));
	            } catch (TwitterException e) {
	                e.printStackTrace();
	            }
	            return null;
	        }
	    }
	    private void loginToTwitter() {
	        // Check if already logged in

	        if (!isTwitterLoggedInAlready()) {
	            new LoginHere().execute();
	        } else {
	            // user already logged into twitter
	            Toast.makeText(getApplicationContext(),
	                    "Already Logged into twitter", Toast.LENGTH_LONG).show();
	        }
	    }

	    /**
	     * Function to update status
	     * */
	    class updateTwitterStatus extends AsyncTask<String, String, String> {

	        /**
	         * Before starting background thread Show Progress Dialog
	         * */
	        @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	            pDialog = new ProgressDialog(K_2013KUCP1029_Login.this);
	            pDialog.setMessage("Updating to twitter...");
	            pDialog.setIndeterminate(false);
	            pDialog.setCancelable(false);
	            pDialog.show();
	        }

	        /**
	         * getting Places JSON
	         * */
	        protected String doInBackground(String... args) {
	            Log.d("Tweet Text", "> " + args[0]);
	            String status = args[0];
	            try {
	                ConfigurationBuilder builder = new ConfigurationBuilder();
	                builder.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
	                builder.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);

	                // Access Token
	               // String access_token = mSharedPreferences.getString(PREF_KEY_OAUTH_TOKEN, "");

	                String access_token="1857306817-43t9RgxNCWvVaNcotcPdFO7UcA9KpJhnMt0jAUB";
	                // Access Token Secret
	               // String access_token_secret = mSharedPreferences.getString(PREF_KEY_OAUTH_SECRET, "");

	                String access_token_secret="oYwrem82E2gp8dBVqj7g46H9fqb1heu3IkLc7ugyddCRm";

	                AccessToken accessToken = new AccessToken(access_token, access_token_secret);
	                Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

	                // Update status
	                twitter4j.Status response = twitter.updateStatus(status);

	                Log.d("Status", "> " + response.getText());
	            } catch (TwitterException e) {
	                // Error in updating status
	                Log.d("Twitter Update Error", e.getMessage());
	            }
	            return null;
	        }

	        /**
	         * After completing background task Dismiss the progress dialog and show
	         * the data in UI Always use runOnUiThread(new Runnable()) to update UI
	         * from background thread, otherwise you will get error
	         * **/
	        protected void onPostExecute(String file_url) {
	            // dismiss the dialog after getting all products
	            pDialog.dismiss();
	            // updating UI from Background Thread
	            runOnUiThread(new Runnable() {
	                @Override
	                public void run() {
	                    Toast.makeText(getApplicationContext(),
	                            "Status tweeted successfully", Toast.LENGTH_SHORT)
	                            .show();
	                    // Clearing EditText field
	                   txtUpdate.setVisibility(View.INVISIBLE);
	                    lblUpdate.setVisibility(View.INVISIBLE);
	                    btnUpdateStatus.setVisibility(View.INVISIBLE);
	                }
	            });
	        }

	    }

	    /**
	     * Function to logout from twitter
	     * It will just clear the application shared preferences
	     * */
	    private void logoutFromTwitter() {
	        // Clear the shared preferences
	        SharedPreferences.Editor e = mSharedPreferences.edit();
	        e.remove(PREF_KEY_OAUTH_TOKEN);
	        e.remove(PREF_KEY_OAUTH_SECRET);
	        e.remove(PREF_KEY_TWITTER_LOGIN);
	        e.commit();

	        // After this take the appropriate action
	        // I am showing the hiding/showing buttons again
	        // You might not needed this code


	        Intent i = new Intent(this,K_2013KUCP1029_ShareApp.class);
	        startActivity(i);
	        finish();
	    }

	    /**
	     * Check user already logged in your application using twitter Login flag is
	     * fetched from Shared Preferences
	     * */
	    private boolean isTwitterLoggedInAlready() {
	        // return twitter login status from Shared Preferences
	        return mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);
	    }

	    protected void onResume() {
	        super.onResume();
	    }
}
