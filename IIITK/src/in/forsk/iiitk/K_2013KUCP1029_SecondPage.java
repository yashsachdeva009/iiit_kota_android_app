package in.forsk.iiitk;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.Session;
import com.facebook.Session.StatusCallback;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;

public class K_2013KUCP1029_SecondPage extends Activity {

	private Facebook facebook;
	private AsyncFacebookRunner mAsyncRunner;
	Context context;
	StatusCallback statusCallback;

	// Reference of BUTTON

	Button facebookPostButton, logOutButton;

	private static final String[] PERMISSIONS = new String[] { "public_profile" };
	private static final String TOKEN = "access_token";
	private static final String EXPIRES = "expires_in";
	private static final String KEY = "facebook-credentials";
	private String messageToPost;
	private Editor editor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_k2013kucp1029_second_page);
		context = this;
		
		Bundle extras = getIntent().getExtras();
		messageToPost = extras.getString("app_link");

		facebook = new Facebook(getString(R.string.app_id));
		mAsyncRunner = new AsyncFacebookRunner(facebook);

	}

	@SuppressWarnings("deprecation")
	public void logoutFromFacebook() {

		Session session = Session.getActiveSession();
		if (session != null) {

			if (!session.isClosed()) {
				session.closeAndClearTokenInformation();
				// clear your preferences if saved

				// editor.clear();
				// editor.commit();

				Intent intent = new Intent(K_2013KUCP1029_SecondPage.this, K_2013KUCP1029_ShareApp.class);

				startActivity(intent);
				finish();
			} else {

				session.closeAndClearTokenInformation();

				Intent intent = new Intent(K_2013KUCP1029_SecondPage.this, K_2013KUCP1029_ShareApp.class);

				startActivity(intent);
				finish();

			}
		} else {

			session = new Session(context);
			Session.setActiveSession(session);

			session.closeAndClearTokenInformation();
			// clear your preferences if saved

			// editor.clear();
			// editor.commit();

			Intent intent = new Intent(K_2013KUCP1029_SecondPage.this, K_2013KUCP1029_ShareApp.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			startActivity(intent);
			finish();
		}

	}

	@SuppressWarnings("deprecation")
	public boolean saveCredentials(Facebook facebook) {
		editor = getApplicationContext().getSharedPreferences(KEY,
				Context.MODE_PRIVATE).edit();
		editor.putString(TOKEN, facebook.getAccessToken());
		editor.putLong(EXPIRES, facebook.getAccessExpires());
		return editor.commit();
	}

	@SuppressWarnings("deprecation")
	public boolean restoreCredentials(Facebook facebook) {
		SharedPreferences sharedPreferences = getApplicationContext()
				.getSharedPreferences(KEY, Context.MODE_PRIVATE);
		facebook.setAccessToken(sharedPreferences.getString(TOKEN, null));
		facebook.setAccessExpires(sharedPreferences.getLong(EXPIRES, 0));
		return facebook.isSessionValid();
	}

	@SuppressWarnings("deprecation")
	public void loginAndPostToWall() {

		facebook.authorize(this, PERMISSIONS, new DialogListener() {

			@Override
			public void onComplete(Bundle values) {
				// TODO Auto-generated method stub

				saveCredentials(facebook);
				if (messageToPost != null) {
					postToWall(messageToPost);
				}

			}

			@Override
			public void onFacebookError(FacebookError e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onError(DialogError e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub

			}
		});
	}

	@SuppressWarnings("deprecation")
	public void postToWall(String message) {
		final Bundle parameters = new Bundle();
		parameters.putString("message", message);
	//	parameters.putString("description", "topic share");
		try {
			mAsyncRunner.request("me", new RequestListener() {

				@Override
				public void onMalformedURLException(MalformedURLException e,
						Object state) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onIOException(IOException e, Object state) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onFileNotFoundException(FileNotFoundException e,
						Object state) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onFacebookError(FacebookError e, Object state) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onComplete(String response, Object state) {
					// TODO Auto-generated method stub
					try {
						facebook.request("me");
						response = facebook.request("me/feed", parameters,
								"POST");
						Log.d("Tests", "got response: " + response);

						if (response == null || response.equals("")
								|| response.equals("false")) {
							Toast.makeText(context, "Blank Response",
									Toast.LENGTH_LONG).show();
						} else {

							K_2013KUCP1029_SecondPage.this.runOnUiThread(new Runnable() {

								@Override
								public void run() {
									// TODO Auto-generated method stub
									Toast.makeText(
											context,
											"Message posted to your facebook wall!",
											Toast.LENGTH_LONG).show();
								}
							});

						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

		} catch (Exception e) {
			showToast("Failed to post to wall!");
			e.printStackTrace();
		}
	}

	private void showToast(String message) {
		// Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT)
		// .show();
	}

	public void post() {

		restoreCredentials(facebook);
		// messageToPost = "Test From Android App";

		String message = "www.forsk.in";

		if (!facebook.isSessionValid()) {
			loginAndPostToWall();
		} else {
			postToWall(message);
		}

	}

}
