package in.forsk.iiitk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class K_2013KUCP1029_SendSMS extends Activity {

	EditText phoneno;
	Button sendmsg;
	String messageToPost;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_k2013kucp1029_send_sms);
		Bundle extras = getIntent().getExtras();
		messageToPost = extras.getString("app_link");

		phoneno = (EditText) findViewById(R.id.getmobilenumber);
		sendmsg = (Button) findViewById(R.id.sendingsms);

		sendmsg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String num = phoneno.getText().toString();
				if (num.isEmpty())
					Toast.makeText(getApplicationContext(),
							"Please enter a phone number", 3000);
				// TODO Auto-generated method stub
				try {
					SmsManager.getDefault().sendTextMessage(num, null,
							""+messageToPost, null, null);
					Toast.makeText(getApplicationContext(), "Message Sent",Toast.LENGTH_SHORT);
					Intent i = new Intent(K_2013KUCP1029_SendSMS.this,K_2013KUCP1029_ShareApp.class);
					startActivity(i);
					finish();
				} catch (Exception e) {
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							K_2013KUCP1029_SendSMS.this);
					AlertDialog dialog = alertDialogBuilder.create();
					dialog.setMessage(e.getMessage());
					dialog.show();
				}
			}
		});
	}

}
