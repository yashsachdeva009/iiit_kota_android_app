package in.forsk.iiitk;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;

public class K_2013KUCP1029_ShareApp extends Activity {
	RelativeLayout loadTwitter, loadfacebook,loadEmail,loadSMS;
	TextView shareTheApp,myFacebook,myTwitter,myEmail,mySMS;
	String name, email, access_token;

	JSONObject profile;
	private Facebook facebook;
	private AsyncFacebookRunner mAsyncRunner;
	private SharedPreferences mpref;
	Context context;
	String messageToPost,link,msg;
	// BUTTON Reference

	Button facebookLoginButton;
//
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_k2013kucp1029_share_app);
		context=this;


		
		//setting font
		shareTheApp=(TextView)findViewById(R.id.shareTheApp);
		myFacebook=(TextView)findViewById(R.id.MyFacebook);
		myTwitter=(TextView)findViewById(R.id.MyTwitter);
		myEmail=(TextView)findViewById(R.id.MyEmail);
		mySMS=(TextView)findViewById(R.id.MySMS);
		
		Typeface tf = Typeface.createFromAsset(getAssets(),"century_gothic.ttf");
		
		shareTheApp.setTypeface(tf);
		myFacebook.setTypeface(tf);
		myTwitter.setTypeface(tf);
		myEmail.setTypeface(tf);
		mySMS.setTypeface(tf);
		
		
		//facebook and twitter
		facebook = new Facebook(getString(R.string.app_id));
		mAsyncRunner = new AsyncFacebookRunner(facebook);

		
		loadTwitter = (RelativeLayout) findViewById(R.id.loadtwitter);
		loadfacebook = (RelativeLayout) findViewById(R.id.facebookk);
		loadEmail = (RelativeLayout) findViewById(R.id.ShareEmail);
		loadSMS = (RelativeLayout) findViewById(R.id.ShareSMS);
		
		
		try {
			JSONObject obj = new JSONObject(loadJSONFromAsset());
			link = (String)obj.getString("link");
			Toast.makeText(getApplicationContext(), link, Toast.LENGTH_LONG);
			msg= (String)obj.getString("aboutUs");
			messageToPost=msg+"  "+link;
		} catch (JSONException e) {

		}
	
		loadTwitter.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(K_2013KUCP1029_ShareApp.this, K_2013KUCP1029_Login.class);
				i.putExtra("app_link", messageToPost);
				startActivity(i);
			}
		});
		
		loadEmail.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*Intent intent = new Intent();
				intent.setAction(Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_TEXT, messageToPost);
				startActivity(intent);*/
				
				Intent i = new Intent(K_2013KUCP1029_ShareApp.this, K_2013KUCP1029_SendMail.class);
				i.putExtra("app_link", messageToPost);
				startActivity(i);
			
			}
		});
		
		loadSMS.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(K_2013KUCP1029_ShareApp.this, K_2013KUCP1029_SendSMS.class);
				i.putExtra("app_link", messageToPost);
				startActivity(i);
			}
		});
		if(loadfacebook!=null){
		loadfacebook.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				loginToFacebook();
			}
		});
	}
	}

	//for checking if the login occurs successfully
	
	@SuppressWarnings("deprecation")
	public JSONObject getProfileInformation() {
		mAsyncRunner.request("me", new RequestListener() {
			@Override
			public void onComplete(String response, Object state) {
				Log.d("Profile", response);
//				String json = response;
			    try {
//					profile = new JSONObject(json);
//					Log.e("JSON STRING", profile.toString());
//					// getting name of the user
//					name = profile.getString("first_name");
//					Log.e("name :", name);
//					// getting email of the user
//					email = profile.getString("email");
//					Log.e("email :", email);
//
					Intent intent = new Intent(K_2013KUCP1029_ShareApp.this,
							K_2013KUCP1029_SecondPage.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

					intent.putExtra("recvd_data", profile.toString());
					intent.putExtra("app_link",link);

					startActivity(intent);
					finish();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onIOException(IOException e, Object state) {
			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e,
					Object state) {
			}

			@Override
			public void onMalformedURLException(MalformedURLException e,
					Object state) {
			}

			@Override
			public void onFacebookError(FacebookError e, Object state) {
			}
		});
		return profile;
	}

	

	
	@Deprecated
	public void loginToFacebook() {

		mpref = getPreferences(MODE_PRIVATE);
		access_token = mpref.getString("access_token", null);

		long expires = mpref.getLong("access_expires", 0);

		if (access_token != null) {
			facebook.setAccessToken(access_token);
		}
		if (expires != 0) {
			facebook.setAccessExpires(expires);
		}
		if (!facebook.isSessionValid()) {

			facebook.authorize(this, new String[] { "email" },
					new DialogListener() {

						@Override
						public void onFacebookError(FacebookError e) {
							// TODO Auto-generated method stub
						}

						@Override
						public void onError(DialogError e) {
							// TODO Auto-generated method stub
						}

						/*
						 * THIS IS A ON COMPLETE METHOD OF DIALOG LISTENER WHICH
						 * WILL HELP IN LOGIN THE APPLICATION
						 */

						@Override
						public void onComplete(Bundle values) {
							// TODO Auto-generated method stub

							// Function to handle complete event
							// Edit Preferences and update facebook acess_token

							// JSONObject jsonProfile=getProfileInformation();

							K_2013KUCP1029_PreferenceManager.getInstance(context)
									.setAcessToken(facebook.getAccessToken());

							Editor editor = mpref.edit();

							editor.putLong("access_expires",
									facebook.getAccessExpires());

							editor.commit();
							getProfileInformation();
							}

						@Override
						public void onCancel() {
							// TODO Auto-generated method stub
						}
					});
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		facebook.authorizeCallback(requestCode, resultCode, data);
	}

	private void showToast(String message) {
		// Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT)
		// .show();
	}

	public String loadJSONFromAsset() {
		String json = null;
		try {

			InputStream is = getAssets().open("ShareApp.json");

			int size = is.available();

			byte[] buffer = new byte[size];

			is.read(buffer);

			is.close();

			json = new String(buffer, "UTF-8");

		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
		return json;

	}
		
}
